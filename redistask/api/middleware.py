from time import sleep
from urllib import response
from django.shortcuts import render,HttpResponse
import redis

def my_middleware(get_response):
    print("Inside Middleware")
    r=redis.Redis()
    def Blocking_api(request):
        x_forwarded_for = request.META.get("HTTP_X_FORWARDED_FOR")
        if x_forwarded_for:
            ip = x_forwarded_for.split(",")[0]
        else:
            ip = request.META.get("REMOTE_ADDR")
        if r.exists(ip):
            r.incr(ip,1)
        else:
            r.mset({ip:0})
            r.expire(ip,60)
        print("Sending Request to API")
        ip_count=int(r.get(ip))
        if ip_count>4:
            raise Exception("To Many Requests ")
        response = get_response(request)
        print("API Executed")
        return response

    return Blocking_api