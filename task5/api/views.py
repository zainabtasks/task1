from django import views
from django.shortcuts import redirect
from rest_framework import viewsets
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.authentication import BasicAuthentication
from rest_framework_simplejwt.authentication import JWTAuthentication
from django.contrib.auth.models import User
from api.serializers import CurrentUserSerializer
from rest_framework import viewsets
from rest_framework.generics import GenericAPIView
from rest_framework.mixins import ListModelMixin, UpdateModelMixin

from rest_framework.decorators import (
    api_view,
    permission_classes,
    authentication_classes,
)

# Create your views here.


class List_users(GenericAPIView, ListModelMixin):
    queryset = User.objects.all()
    serializer_class = CurrentUserSerializer
    permission_classes = [IsAdminUser]
    authentication_classes = [JWTAuthentication]

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)




@api_view(["GET"])
@permission_classes([IsAuthenticated])
@authentication_classes([BasicAuthentication])
def get_current_user(request):
    user_id = request.user.id
    response = redirect("/api/update/" + str(user_id))
    return response


class update_user(GenericAPIView, UpdateModelMixin):
    permission_classes = [IsAuthenticated]
    authentication_classes = [BasicAuthentication]
    queryset = User.objects.all()
    serializer_class = CurrentUserSerializer

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)
