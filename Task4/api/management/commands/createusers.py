from django.contrib.auth.models import User
from django.core.management.base import BaseCommand
from django.utils.crypto import get_random_string


class Command(BaseCommand):
    help = "Create random users"

    def add_arguments(self, parser):
        parser.add_argument(
            "Users_Count", type=int, help="Total Number Of Users You Want to create"
        )

    def handle(self, *args, **kwargs):
        Count = kwargs["Users_Count"]
        for i in range(Count):
            User.objects.create_user(
                username=get_random_string(length=20), email="", password="123"
            )
