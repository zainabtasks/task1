# from datetime import timezone
import datetime
from django.core.management.base import BaseCommand
from django.utils.crypto import get_random_string
from api.models import TimeRecord3
from pytz import timezone


class Command(BaseCommand):
    help = "Create Time Objects"

    def add_arguments(self, parser):
        parser.add_argument(
            "Time_Obj_Count",
            type=int,
            help="Total Number Of Time Obj you want to create",
        )

    def handle(self, *args, **kwargs):
        Count = kwargs["Time_Obj_Count"]
        for i in range(Count):
            time1 = datetime.datetime.now()
            time1 = time1.astimezone(timezone("US/Pacific"))
            time = TimeRecord3(time=time1, timezone="PST")
            time.save()
