from time import time
from .models import TimeRecord3
from django.shortcuts import render, HttpResponse
import datetime
from pytz import timezone


def my_cron_job():

    start = TimeRecord3.objects.get(id=1)
    start = start.loopiter
    end = start + 10
    timezone_data = TimeRecord3.objects.get(id=end)
    timezone_data = timezone_data.timezone
    if timezone_data == "PST":
        for i in range(start + 1, end + 1):
            obj = TimeRecord3.objects.get(id=i)
            obj.timezone = "UTC"
            obj.save()

    if timezone_data == "UTC":
        for i in range(start + 1, end + 1):
            obj = TimeRecord3.objects.get(id=i)
            obj.timezone = "PST"
            obj.save()
    if end < 100:
        time_obj = TimeRecord3.objects.get(id=1)
        time_obj.loopiter = end
        time_obj.save()
    elif end == 100:
        time_obj = TimeRecord3.objects.get(id=1)
        time_obj.loopiter = 0
        time_obj.save()
