from django.db import models


# Create your models here.


class TimeRecord3(models.Model):
    time = models.DateTimeField()
    timezone = models.TextField(max_length=30, default="timezone")
    loopiter = models.IntegerField(default=0)
