# Generated by Django 4.0.6 on 2022-07-22 06:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("api", "0008_timerecord2"),
    ]

    operations = [
        migrations.CreateModel(
            name="TimeRecord3",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("time", models.DateTimeField()),
            ],
        ),
    ]
