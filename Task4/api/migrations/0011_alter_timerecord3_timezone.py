# Generated by Django 4.0.6 on 2022-07-24 14:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("api", "0010_timerecord3_timezone"),
    ]

    operations = [
        migrations.AlterField(
            model_name="timerecord3",
            name="timezone",
            field=models.TextField(default="timezone", max_length=30),
        ),
    ]
