# Generated by Django 4.0.6 on 2022-07-20 09:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("registrationapi", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="user",
            name="username",
            field=models.CharField(max_length=30, null=True, unique=True),
        ),
    ]
