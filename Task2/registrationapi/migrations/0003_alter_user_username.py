# Generated by Django 4.0.6 on 2022-07-20 10:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("registrationapi", "0002_user_username"),
    ]

    operations = [
        migrations.AlterField(
            model_name="user",
            name="username",
            field=models.EmailField(max_length=254, null=True, unique=True),
        ),
    ]
