from django.db import models
from django.contrib.auth.models import AbstractUser, AbstractBaseUser
from .manager import UserManager

# Create your models here.
class User(AbstractUser):
    username = models.CharField(max_length=30, unique=True, null=True)
    secondaryemail = models.EmailField(null=True, blank=True)
    gender = models.CharField(max_length=20, null=True, blank=True)
    objects = UserManager
    REQUIRED_FIELDS = []
