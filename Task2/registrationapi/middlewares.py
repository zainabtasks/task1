from urllib import response


def my_middleware(get_response):
    print("Inside Middleware")

    def save_request_info(request):
        x_forwarded_for = request.META.get("HTTP_X_FORWARDED_FOR")
        if x_forwarded_for:
            ip = x_forwarded_for.split(",")[0]
        else:
            ip = request.META.get("REMOTE_ADDR")
        with open("registrationapi/info.txt", "a") as f:
            f.write("IP Address: " + ip + "\n")
            f.write("Request Method: " + request.method + "\n")
        print("Sending Request to API")
        response = get_response(request)
        print("API Executed")
        return response

    return save_request_info
