from cProfile import Profile
import imp
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.core.exceptions import ObjectDoesNotExist
from .forms import SignupForm, LoginForm
from django.contrib.auth import login, authenticate, logout
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def register_request(request):
    if request.method == "POST":
        form = SignupForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)

            return render(request=request, template_name="registrationapi/home.html")

    form = SignupForm()
    return render(
        request=request,
        template_name="registrationapi/register.html",
        context={"register_form": form},
    )


@csrf_exempt
def login_request(request):
    if request.method == "POST":

        form = LoginForm()
        username = request.POST.get("username")
        password = request.POST.get("password")
        print(username)
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return render(
                request=request, template_name="registrationapi/loginhome.html"
            )

    form = LoginForm()
    return render(
        request=request,
        template_name="registrationapi/login.html",
        context={"login_form": form},
    )


def logout_request(request):
    logout(request)

    return render(request=request, template_name="registrationapi/home.html")


def basic(request):
    return render(request=request, template_name="registrationapi/basic.html")


def home_page_loader(request):
    return render(request=request, template_name="registrationapi/home.html")
