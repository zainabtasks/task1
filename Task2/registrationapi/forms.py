from dataclasses import field
from django import forms
from django.contrib.auth.forms import UserCreationForm
from registrationapi.models import User
from pkg_resources import require
from django import forms

# Create your forms here.


class SignupForm(UserCreationForm):

    username = forms.CharField()
    email = forms.EmailField()
    password1 = forms.CharField(max_length=32, widget=forms.PasswordInput)
    password2 = forms.CharField(
        label="Confirm Password", max_length=32, widget=forms.PasswordInput
    )
    secondaryemail = forms.EmailField()
    gender = forms.CharField(max_length=20)

    class Meta:
        model = User
        fields = (
            "username",
            "email",
            "password1",
            "password2",
            "secondaryemail",
            "gender",
        )

    def save(self, commit=True):
        user = super(SignupForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        user.secondaryemail = self.cleaned_data["secondaryemail"]
        user.gender = self.cleaned_data["gender"]

        print("aaaaaaaaaa")
        print(self.cleaned_data["password1"])
        user.password2 = self.cleaned_data["password2"]
        if commit:
            user.save()
        return user


class LoginForm(forms.Form):
    username = forms.CharField(required=True)

    password = forms.CharField(max_length=32, widget=forms.PasswordInput, required=True)
