from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

# Create your models here.
class userprofile(models.Model):
    name = models.CharField(max_length=200)
    phone = PhoneNumberField()
    photo = models.ImageField(upload_to="TASK1/media/")
