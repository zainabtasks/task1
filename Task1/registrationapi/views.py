from cProfile import Profile
import imp
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.core.exceptions import ObjectDoesNotExist
from registrationapi.models import userprofile
from .forms import SignupForm, LoginForm, Userprofile
from django.contrib.auth import login, authenticate, logout
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def register_request(request):
    if request.method == "POST":
        form = SignupForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)

            return render(
                request=request, template_name="registrationapi/loginhome.html"
            )

    form = SignupForm()
    return render(
        request=request,
        template_name="registrationapi/register.html",
        context={"register_form": form},
    )


@csrf_exempt
def login_request(request):
    if request.method == "POST":

        form = LoginForm()
        username = request.POST.get("username")
        password = request.POST.get("password")
        print(username)
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)

            return render(
                request=request, template_name="registrationapi/loginhome.html"
            )

    form = LoginForm()
    return render(
        request=request,
        template_name="registrationapi/login.html",
        context={"login_form": form},
    )


def logout_request(request):
    logout(request)

    return render(request=request, template_name="registrationapi/home.html")


def basic(request):
    return render(request=request, template_name="registrationapi/basic.html")


@csrf_exempt
def user_profile_view(request):
    print(request.FILES)
    if request.method == "POST":
        form = Userprofile(request.POST, request.FILES)
        print(request.FILES)
        if form.is_valid():
            form.save()
            return render(request=request, template_name="registrationapi/home.html")
    else:
        form = Userprofile()
    return render(request, "registrationapi/profile.html", {"form": form})


csrf_exempt


def update_view(request, pk):
    try:
        obj = userprofile.objects.get(id=pk)
    except ObjectDoesNotExist:
        return HttpResponse("Exception: Data not found")
    form = Userprofile(request.POST, request.FILES, instance=obj)
    if form.is_valid():
        form.save()
        return render(request=request, template_name="registrationapi/home.html")
    return render(request, "registrationapi/editprofile.html", {"form": form})


def home_page_loader(request):
    return render(request=request, template_name="registrationapi/home.html")
