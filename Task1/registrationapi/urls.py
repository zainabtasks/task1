from django.urls import path
from . import views

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path("signup/", views.register_request),
    path("login/", views.login_request),
    path("logout/", views.logout_request),
    path("basic/", views.basic),
    path("profile/", views.user_profile_view),
    path("profile/<int:pk>", views.update_view),
    path("", views.home_page_loader),
]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
