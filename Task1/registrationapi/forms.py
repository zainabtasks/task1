from dataclasses import field
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from pkg_resources import require
from django import forms
from requests import request
from .models import userprofile

# Create your forms here.


class SignupForm(UserCreationForm):

    username = forms.CharField(required=True)
    email = forms.EmailField(required=True)
    password1 = forms.CharField(
        label="Password", max_length=32, widget=forms.PasswordInput, required=True
    )
    password2 = forms.CharField(
        label="Confirm Password",
        max_length=32,
        widget=forms.PasswordInput,
        required=True,
    )

    class Meta:
        model = User
        fields = ("username", "email", "password1", "password2")

    def save(self, commit=True):
        user = super(SignupForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        user.password2 = self.cleaned_data["password2"]
        if commit:
            user.save()
        return user


class LoginForm(forms.Form):
    username = forms.CharField(required=True)

    password = forms.CharField(max_length=32, widget=forms.PasswordInput, required=True)


class Userprofile(forms.ModelForm):
    class Meta:
        model = userprofile
        fields = ("name", "phone", "photo")
